# Example Node.js Serverless Project

This is an example project to learn how to write node.js applications for ServerlessJS and deploy to AWS.

## Getting Started

Install serverless and set your credentials. 
```bash
npm install serverless -g
serverless config credentials --provider aws --key YOUR_KEY_HERE --secret YOUR_SECRET_HERE
```
Your credentials can be created here:
https://console.aws.amazon.com/iam/home#/security_credentials

## Deploy the app

```bash
sls deploy
```

The URL provided by the serverless output will hit your new API.