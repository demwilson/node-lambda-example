'use strict';
const algorithm = (arr) => {
  // John
  return arr.reduce(
      (
          total,          // 74
          char,           // 'o'
      ) => {
          return total + char.charCodeAt(0);
      }, 
      0
  );
};

const userStats = (user) => {
  // Run algorithm
  const value = algorithm(user.split(''));
  // return outputted data
  return value;
}

module.exports.userInfo = (event, context, callback) => {
  const username = event.queryStringParameters.username || '';
  const responseData = userStats(username);

  const response = {
    statusCode: 200,
    headers: {
      'Access-Control-Allow-Origin': '*', // Required for CORS support to work
    },
    body: responseData,
  };

  callback(null, response);
};
